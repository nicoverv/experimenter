# Experimenter

Experimenter is a framework taking out the complexity of running experiments
over a grid of parameters. As the number of experiments typically grows large
quickly, the experiments can be run somewhere and the user can monitor the
progress using the HTML interface.

## Example

A method `f` is dependent on three parameters: `p1`, `p2` and `p3`
and the values `x`, `y` and a timing for each combination of parameters is
needed. Parameters `p2` and `p3` only appear in certain combinations. To remove
statistical noise, all experiments are to be repeated `N` times.

The experiment framework can be used as follows:

1. An experiment script is created, say `methodXY.m`, containing for example:

        A = rand(p1);
        tic
        [x, y] = f(A, p2, p3);
        time = toc;
   All parameters have the value for the current experiment when the
   script is called. The variable `iteration` contains the iteration
   number. The variables `params` and `vars` are used by the program and
   cannot be used by the user.

2. A experiment file `experimentXY.m` is created with the following content:

        addpath('/path/to/some/toolbox/if/necessary/')
       
        experiment = Experimenter();
        experiment.addParameter('p1', 1:3);
        experiment.addParameter('p2', [1 2 3 4], 'p3', {'a', 'b', 'test', 'd'});
        experiment.Repetitions = N;
        experiment.Outputs = {'x', 'y', 'time'};
        experiment.Function = 'methodXY';
        experiment.Title = 'TestF';
        experiment.RunInParallel = true; % (Optional, default is true);
        experiment.NumWorkers = 4; % (Optional, if not present the system
                                   %  default is taken.)
       
        results = experiment.run();
   After completion, `results` will be a struct of dimensions `Repetitions x
   length(p1) x length(p2)`. (If Repetitions = 1, this dimension is ommited).
   The results will be saved in a mat-file called `TestF.mat` (i.e. the title).
   Each element in the struct will contain the parameter values and the outputs.

3. The results can analyzed as follows (assuming `x` is a single value and `y`
   is a vector of length 7):

        X = reshape([results.x], size(results));
        X = squeeze(mean(X));
        Y = reshape([results.y], [size(results) 7]);

To monitor the progress of the experiments, a reporter function is builtin. This
is an HTML form called `reporter.html` that depends on `reporter.css`,
`reporter.js` and `d3.min.js`. These files can be placed in any directory. If
remote access is required, the files should be placed in accessible folder, e.g.
`~/public_html/`. The directory can be set using:

    experiment.Path = '~/public_html/'; % (This is the default).

In this directory a file called `header.json` and `TestF.csv` (the title) are
constructed. The former contains some bookkeeping, the latter contains the
progress of the experiments. Both files will be read by the HTML frontend. The
frontend will also give an estimate of the remaining time based on intelligent
averaging: the time left for each parameter combination is estimated separately.
The time estimate will be less accurate if the experiments are executed in
parallel, as the order of the experiments is not fixed by `parfor`.
