var experimentfiles = [];
var fmt = d3.time.format("%c");
var timefmt = d3.time.format("%X");
var nbActiveExperiments = 0;

function loadexperiments(file) {

    d3.json(file, function(error, d) {
        console.log(d);

        var exp  = d3.select(".content")
            .append("div");

        exp.attr("class", "experiment");
        exp.append("h1").text(d.title);
        exp.append("h2").text("Details");
        var tbl = exp.append("table");
        var row = tbl.append("tr");
        row.append("td")
            .style("width", "300px")

            .text("Start time");

        row.append("td")
            .text(fmt(new Date(d.startTime)));
        row = tbl.append("tr");
        row.append("td").text("Number of experiments");
        row.append("td")
            .text(d.nbExperiments);
        row = tbl.append("tr");
        row.append("td").text("Number of repetitions");
        row.append("td")
            .text(d.nbRepetitions);
        exp.append("h2").text("Progress");
        tbl = exp.append("table").attr("id", d.file + "_progress");
        row = tbl.append("tr");
        row.append("td").text("Experiments done");
        row.append("td")
            .text("0 (0%)")
            .attr("id", d.file +  "_expdone");
        row = tbl.append("tr");
        row.append("td").text("Errors");
        row.append("td")
            .text("0")
            .attr("id", d.file +  "_errors");
        row = tbl.append("tr");
        row.append("td")
            .style("width", "300px")
            .text("Estimated time left");
        row.append("td")
            .text("0 s")
            .attr("id", d.file +  "_timeleft");
        row = tbl.append("tr");
        row.append("td")
            .style("width", "300px")
            .text("Estimated end time");
        row.append("td")
            .text("n.a.")
            .attr("id", d.file +  "_endtime");
        exp.append("div")
            .attr("class", "meter")
            .append("span")
            .style("width", "0")
            .attr("id", d.file + "bar");

        d.done = false;
        d.total = d.nbExperiments * d.nbRepetitions;
        d.workers = +d.workers;
        experimentfiles.push(d);

        update();
    });
}

function processResultFile(error, rows, exp)  {
    var bar =  d3.selectAll("#" + file +"bar");
    bar.style("width", rows.length / exp.total * 100 +
              "%");
    if (rows.some(function (d) { return d.error == 1; }))
        bar.attr("class", "error");
    

    var expdone = d3.nest()
        .key(function (d) {return d.paramid;})
        .rollup(function (leaves) {
            var avg = d3.mean(leaves, function(l) {
                return l.time;});
            var errs = d3.sum(leaves, function(l) {
                return l.inerror;
            });
            return { "done" : leaves.length,
                     "avg_time" : avg,
                     "total": leaves.length * avg,
                     "estleft": avg*(exp.nbRepetitions - leaves.length)};
        })
        .entries(rows);
    console.log(expdone);
    var avgtime = d3.sum(expdone, function (x) { return x.values.total; });
    avgtime /= rows.length;

    var errors = d3.sum(rows, function (d) { return +d.error; });

    var timeleft = avgtime * (exp.nbExperiments -
                              expdone.length)*exp.nbRepetitions;
    timeleft +=  d3.sum(expdone, function (x) { return x.values.estleft; });
    timeleft /= exp.workers;
    var endtime = new Date();
    endtime = new Date(endtime.getTime() + timeleft*1000);

    // Update fields
    d3.select("#" + file + "_expdone")
        .text(rows.length + " (" + d3.format(".1%")(rows.length / exp.total) + ")");
    d3.select("#" + file + "_timeleft")
        .text(seconds2string(timeleft));
    d3.select("#" + file + "_endtime")
        .text(d3.time.format("%c")(endtime));
    d3.select("#" + file + "_errors")
        .text(errors);

    // Update fields if done
    if (rows.length == exp.total && ! exp.done)  {
        exp.done = true;
        var tbl = d3.select("#" + file + "_progress");
        tbl.selectAll("tr")
            .remove();
        var row = tbl.append("tr");
        row.append("td")
            .style("width", "300px")
            .text("End time");
        row.append("td")
            .text(fmt(rows[rows.length-1].endtime));
        row = tbl.append("tr");
        row.append("td")
            .style("width", "300px")
            .text("Errors");
        row.append("td")
            .text(errors);
        nbActiveExperiments--;
    } 
}

function update() {
    for(var i in experimentfiles) {
        file = experimentfiles[i].file;
        var exp =  experimentfiles[i];
        d3.csv(file + ".csv", function(d) {
            return {
                current: +d.exp,
                error: +d.error,
                percentage: +d.exp/+d.total*100,
                endtime: new Date(+d.endtime),
                timeleft: new Date(+d.timeleft),
                paramid: +d.paramid,
                time: +d.time
            };
        }, function(error, rows) { 
            return processResultFile(error, rows, exp); 
        });
    }
    setTimeout(update, 1000);
}

function seconds2string(sec)  {
    var result = "";
    mins  = Math.floor(sec/60);
    hours = Math.floor(mins/60);
    days  = Math.floor(hours / 24);
    sec   = Math.floor(sec % 60);
    mins =  mins % 60;
    hours = hours % 24;
    if (days > 0) result += days + " days ";
    if (hours > 0) result += hours + " hours ";
    if (mins > 0) result += mins + " mins ";
    result += sec + " s";
    return result;
}
