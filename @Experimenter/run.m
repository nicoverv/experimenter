function results = run(this)
%RUN short discription
%   long description
%   

% Author(s): Nico Vervliet       (Nico.Vervliet@esat.kuleuven.be)
%
% Version History:
% - 2015/01/06   NV      Initial version

% Check correctness 
    
    if this.RunInParallel
        if isempty(gcp('nocreate')) && ~isnan(this.NumWorkers)
            parpool('local', this.NumWorkers);
        end
        pool = gcp;
        this.Workers = pool.NumWorkers;
    else  
        this.Workers = 1;
    end

    % Create tracker files
    Experimenter.savejson([this.Path 'header.json'], createheader(this));
    filepath = [this.Path this.Title '.csv'];
    fid = fopen(filepath, 'w');
    fprintf(fid, 'exp,error,paramid,time,endtime\n');
    fclose(fid);

    % Extract parameters
    parameters = this.Parameters;
    parameters.iteration = 1:this.Repetitions;
    sz = structfun(@length, parameters).';
    N = prod(sz);
    results = cell(1, N);
    
    fprintf('Starting experiments\n');
    fprintf(['Total number of experiments is %d. At 1s per experiment, it will ' ...
             'take %d day(s) %d hour(s) %d min(s) %d s.\n'], N, ...
            floor(N/24/60/60/this.Workers), mod(floor(N/60/60/this.Workers), 24), ...
            mod(floor(N/60/this.Workers), 60), mod(ceil(N/this.Workers), 60));
    
    % Outputs
    outputs = this.Outputs;
    func = this.Function;
    
    if this.RunInParallel
        parfor i = 1:N
            % Start loop timer for end time prediction
            time = tic;
            
            % extract parameter indices
            sub = cell(1, length(sz));
            [sub{:}] = ind2sub(sz, i);
            sub = cat(2, sub{:});
            
            % evaluate the parameters
            params = Experimenter.evalParameters(parameters, sub);
            params.DryRun = this.DryRun;
            
            % perform experiment
            exp_in_error = 0;
            try 
                results{i} = Experimenter.runExperiment(params, outputs, func);
                results{i}.inerror = false;
                results{i}.errorthrown = nan;
            catch err
                exp_in_error = 1;
                results{i} = params;
                results{i}.inerror = true;
                results{i}.errorthrown = err;
                for n = 1:length(outputs)
                    results{i}.(outputs{n}) = nan;
                end 
            end 
            
            % Stop loop timer for end time prediction
            time = toc(time);
            
            % publish status
            Experimenter.publishStatus(filepath, i, exp_in_error, sz, sub, ...
                                       time);
        end 
    else 
        for i = 1:N
            % Start loop timer for end time prediction
            time = tic;
            
            % extract parameter indices
            sub = cell(1, length(sz));
            [sub{:}] = ind2sub(sz, i);
            sub = cat(2, sub{:});
            
            % evaluate the parameters
            params = Experimenter.evalParameters(parameters, sub);
            params.DryRun = this.DryRun;
            
            % perform experiment
            exp_in_error = 0;
            try 
                results{i} = Experimenter.runExperiment(params, outputs, func);
                results{i}.inerror = false;
                results{i}.errorthrown = nan;
            catch err
                rethrow(err)
                exp_in_error = 1;
                results{i} = params;
                results{i}.inerror = true;
                results{i}.errorthrown = err;
                for n = 1:length(outputs)
                    results{i}.(outputs{n}) = nan;
                end 
            end 
            
            % Stop loop timer for end time prediction
            time = toc(time);
            
            % publish status
            Experimenter.publishStatus(filepath, i, exp_in_error, sz, sub, ...
                                       time);
        end
        
    end 

    results = cat(1, results{:});
    results = reshape(results, [sz numel(results)/prod(sz)]);
    if this.Repetitions > 1, 
        n = length(sz);
        results = permute(results, [n 1:n-1 n+1:ndims(results)]);
    end 
    save([this.Title '.mat'], 'results');

    fprintf('Experiments are finished\n');
    fprintf('The results are written to %s.mat\n', this.Title);
    
end
