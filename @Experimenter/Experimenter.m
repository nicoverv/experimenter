%EXPERIMENTER performs experiments over group of parameters.
%
%   EXPERIMENTER is a framework taking out the complexity of running experiments
%   over a grid of parameters. As the number of experiments typically grows
%   large quickly, the experiments can be run somewhere and the user can
%   monitor the progress using the HTML interface.
%
%   A typical use case is the following: 
%
%       A method f is dependent on three parameters: p1, p2 and p3 and the
%       values x, y and a timing for each combination of parameters is needed.
%       Parameters p2 and p3 only appear in certain combinations. To remove
%       statistical noise, all experiments are to be repeated N times.
% 
%   The experiment framework can be used as follows:
%
%       1) An experiment script is created, say 'methodXY.m', containing for
%          example:
%
%              A = rand(p1);
%              tic
%              [x, y] = f(A, p2, p3);
%              time = toc;
%
%          All parameters have the value for the current experiment when the
%          script is called. The variable iteration contains the iteration
%          number. The variables params and vars are used by the program and
%          cannot be used by the user.
%
%       2) A experiment file 'experimentXY.m' is created with the following
%          content:
%
%              addpath('/path/to/some/toolbox/if/necessary/')
%
%              exp = Experimenter();
%              exp.addParameter('p1', 1:3);
%              exp.addParameter('p2', [1 2 3 4], 'p3', {'a', 'b', 'test', 'd'});
%              exp.Repetitions = N;
%              exp.Outputs = {'x', 'y', 'time'};
%              exp.Function = 'methodXY';
%              exp.Title = 'TestF';
%              exp.RunInParallel = true; % (Optional, default is true);
%              exp.NumWorkers = 4; % (Optional, if not present the system
%                                  %  default is taken.)
%
%              results = e.run();
%
%          After completion, results will be a struct of dimensions Repetitions
%          x length(p1) x length(p2). (If Repetitions = 1, this dimension is
%          ommited). The results will be saved in a mat-file called TestF.mat
%          (i.e. the title). Each element in the struct will contain the
%          parameter values and the outputs.
%
%       3) The results can analyzed as follows (assuming x is a single value
%          and y is a vector of length 7):
%
%              X = reshape([results.x], size(results));
%              X = squeeze(mean(X));
%              Y = reshape([results.y], [size(results) 7]);
%
%   To monitor the progress of the experiments, a reporter function is
%   builtin. This is an HTML form called reporter.html that depends on
%   reporter.css, reporter.js and d3.min.js. These files can be placed in any
%   directory. If remote access is required, the files should be placed in
%   accessible folder, e.g. ~/public_html/. The directory can be set using:
%
%       exp.Path = '~/public_html/'; % (This is the default).
%
%   In this directory a file called header.json and TestF.csv (the title) are
%   constructed. The former contains some bookkeeping, the latter contains
%   the progress of the experiments. Both files will be read by the HTML
%   frontend. The frontend will also give an estimate of the remaining time
%   based on intelligent averaging: the time left for each parameter
%   combination is estimated separately. The time estimate will be less
%   accurate if the experiments are executed in parallel, as the order of the
%   experiments is not fixed by parfor.

% Author(s): Nico Vervliet       (Nico.Vervliet@esat.kuleuven.be)
%
% Version History:
% - 2015/02/20   NV      Initial version
classdef Experimenter < handle
    properties
        Repetitions        
        Parameters
        Function 
        Outputs
        Path
        Title
        Workers
        NumWorkers
        RunInParallel
        DryRun
    end
    
    methods 
        
        function this = Experimenter()
            this.Path =  '~/public_html/';
            this.Function = nan;
            this.Parameters = struct();
            this.Outputs = {};
            this.Repetitions = 1;
            this.NumWorkers = nan;
            this.RunInParallel = true;
            this.DryRun = false;
        end
        
        function this = addParameter(this, varargin)
            if (length(varargin) == 2)
                this.Parameters.(varargin{1}) = varargin{2};
            else 
                name = sprintf('group%d', length(fieldnames(this.Parameters)));
                
                if length(unique(cellfun(@length, varargin(2:2:end)))) ~= 1
                    error('Experiments:InvalidFormat', ['All parameters in a ' ...
                                        'group should have the same length.']);
                end
                
                s = struct;
                for k = 2:2:length(varargin)
                    for l = 1:length(varargin{k})
                        if iscell(varargin{k})
                            s(l).(varargin{k-1}) = varargin{k}{l};
                        else 
                            s(l).(varargin{k-1}) = varargin{k}(l);
                        end
                        if k == 2, s(l).exp_grouped = 1; end
                    end
                end
                this.Parameters.(name) = s;
            end
        end 
        
        function set.Repetitions(this, value)
            if value > 0, this.Repetitions = value;
            else this.Repetitions = 1;
            end  
        end  
        
        function set.Function(this, value)
            if isnan(value), this.Function = nan; return; end
            if ~ischar(value)
                error('Experimenter:InvalidFormat', ...
                      'Function should be a script name given as string');
            end 
            if length(value) > 2 && strcmpi(value(end-1:end), '.m')
                value = value(1:end-2);
            end  
            if exist([value  '.m'], 'file') ~= 2
                warning('Experimenter:NotFound', ...
                        'The function ''%s'' is not found.', value);
            end 
            this.Function = value;
        end 
        
        function set.Outputs(this, value)
            if iscell(value)
                this.Outputs = value;
            elseif ischar(value)
                this.Outputs = {value};
            else 
                error('Experimenter:InvalidFormat', ... 
                      'Outputs should be a string or a cell of strings');
            end  
        end 
        
        function set.Path(this, value)
            if exist(value, 'dir')  ~= 7
                warning('Experimenter:NotFound', ...
                        'The directory ''%s'' is not found.', value);
            end 
            if ~isempty(value) && ~(value(end) == '/' || value(end) == '\')
                % TODO: add windows support
                value = [value '/'];
            end 
            this.Path = value;
        end 
        
    end
    
    methods (Access = private)
                
        function result = createheader(this)
            result = struct();
            result.title = this.Title;
            result.file = [this.Title];
            
            Np = cellfun(@(fn) length(this.Parameters.(fn)), ...
                                   fieldnames(this.Parameters));

            result.nbExperiments = prod(Np(:));
            result.nbRepetitions = this.Repetitions;
            result.startTime = Experimenter.getunixtime(now);
            result.workers = this.Workers;
        end
    end
    
    methods (Static)
        
        function savejson(file, data)
            fid = fopen(file, 'w');
            fprintf(fid, Experimenter.object2json(data));
            fclose(fid);
        end

        function json = object2json(data)
            json = '{\n';
            
            fns = fieldnames(data);
            for i = 1:length(fns);
                fn = fns{i};
                allvalues =  '';
                values = data.(fn);
                if ischar(values)
                    values = {values};
                else 
                    values = num2cell(values);
                end
                for k = 1:length(values)
                    value = values{k};
                    if ischar(value)
                        value =  ['"' value '"'];
                    elseif isnumeric(value)
                        value = num2str(value);
                    elseif islogical(value)
                        if value
                            value = 'true';
                        else
                            value = 'false';
                        end
                    else 
                        value = 'error';
                    end
                    allvalues = [allvalues value];
                end
                json = sprintf('%s\t"%s": %s,\n', json, fn, allvalues);
            end
            json = [json(1:end-2) '\n}'];
        end
        
        function ut = getunixtime(tm)
            corr = java.lang.System.currentTimeMillis - ...
                   round(864e5 * (now - datenum('1970', 'yyyy')));
            ut = round(864e5 * (tm - datenum('1970', 'yyyy'))) + corr;
        end
        
        function publishStatus(filename, nb, inerror, sz, sub, duration)
            fields = cell(4,1);
            fields{1} = nb;
            fields{2} = inerror;
            if length(sz) > 1
                sub = num2cell(sub(2:end));
                fields{3} = sub2ind(sz(2:end), sub{:});
            else 
                fields{3} = sub(2);
            end 
            corr = java.lang.System.currentTimeMillis - ...
                   round(864e5 * (now - datenum('1970', 'yyyy')));
            fields{4} = duration;
            fields{5} = round(864e5 * (now - datenum('1970', 'yyyy'))) + corr;
            fid = fopen(filename, 'a');
            fprintf(fid, '%d,%d,%d,%16.8f,%d\n', fields{:});
            fclose(fid);
        end 
        
        function results = runExperiment(params, vars, e_func)
            fns = fieldnames(params);
            for i = 1:length(fns)
                fn = fns{i};
                eval(sprintf('%s = params.(''%s'');', fn, fn));
            end 
            eval(e_func);
            results = Experimenter.collectResults(params, vars);
        end 

        function result = collectResults(p, vars)
            result = p;
            for i = 1:length(vars)
                value = evalin('caller', vars{i});
                result.(vars{i}) = value(:).';
            end 
        end 
        
        function p = evalParameters(params, sub)
            fns = fieldnames(params);
            p = struct;
            for i = 1:length(fns)
                name = fns{i};
                value = params.(name)(sub(i));
                if isstruct(value)
                    fn2 = fieldnames(params.(name));
                    for j = 1:length(fn2);
                        name2 = fn2{j};
                        value2 = value.(name2);
                        p.(name2) = value2;
                    end 
                else 
                    p.(name) = value;
                end 
            end 
        end 
    end 
end

