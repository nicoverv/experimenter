if exist('Experimenter', 'class') == 0
    addpath('../');
end  

clear all

e = Experimenter();
e.addParameter('myvar', 1:3);
e.addParameter('bla', 1:3);
e.addParameter('knots', 1:3, 'boem', 1:3);
e.Function = 'experiment1';
e.Outputs = {'haha', 'hehe'};
e.Title = 'test';
e.Repetitions = 2;
e.RunInParallel = true;

results = e.run();